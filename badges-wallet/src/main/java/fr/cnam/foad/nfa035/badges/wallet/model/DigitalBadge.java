package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge implements Comparable <DigitalBadge>{

    private DigitalBadgeMetadata metadata;
    private File badge;

    private Date begin;
    private Date end;
    private String serial;


    /**
     * Constructeur
     * @param metadata
     * @param badge
     * @param end
     * @param begin
     * @param serial
     */
    public DigitalBadge(String serial, Date begin, Date end, DigitalBadgeMetadata metadata, File badge ) {
        this.metadata = metadata;
        this.badge = badge;
        this.serial = serial;
        this.end = end;
        this.begin = begin;
    }


    /**
     * Getter de la date d'obtention
     * @return Date
     */

    public Date getBegin() {
        return begin;
    }

    /**
     * Setter de la date d'obtention
     * @param begin
     */
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    /**
     * Getter de la date de péremption
     * @return Date
     */

    public Date getEnd() {
        return end;
    }

    /**
     * Setter de la date de péremption
     * @param end
     */

    public void setEnd(Date end) {
        this.end = end;
    }

    /**
     * Getter du code de série du badge
     * @return String
     */
    public String getSerial() {
        return serial;
    }

    /**
     *  Setter du code de série du badge
     * @param serial
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return metadata.equals(that.metadata) && badge.equals(that.badge);

    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge,end,begin);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                ",serial=" + serial+
                ",begin=" + begin+
                ",end=" + end+
                '}';
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return
     */
    @Override
    public int compareTo(DigitalBadge o) {
        return metadata.compareTo(o.getMetadata());
    }
}
